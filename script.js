
// DEFININDO JOGADORES
const jogador1 = "X";
const jogador2=  "O";
//DEFINE DE QUEM É A VEZ
var vezdojogador = jogador1;
//VARIÁVEL PARA SABER SE O JOGO JÁ ACABOU
var fimdojogo= false;
//conta o numero de jogadas
var contadorDeJogadas=0;
// X começa jogando
vezdequem();
// FUNÇÃO PARA ATUALIZAR DE QUEM É A VEZ DE JOGAR, ELA SERÁ CHAMADA NA FUNÇÃO  "SELECIONAR"
function vezdequem(){
    if(fimdojogo) {return;}

    if(vezdojogador == jogador1){

         document.getElementById("indicadorDaVez").innerHTML = "X"; 
    }

    if(vezdojogador == jogador2){

         document.getElementById("indicadorDaVez").innerHTML = "O"; 
    }


}
// Realiza uma jogada
function selecionar(p) {
    // so funciona se o jogo está ativo
     if(fimdojogo) { return;}
     // verifica qual botão foi clickado
     for(var i=0;i<9;i++)
     {
          if(p==i)
          {    //verifica se o botão ta vazio
               if(document.getElementsByClassName("casa")[i].innerHTML==""){
               //verifica de quem é a vez
               if(vezdojogador == jogador1){

                    document.getElementsByClassName("casa")[i].innerHTML = "X";
                    contadorDeJogadas++;
                    vezdojogador=jogador2;
   
               }
           
              else{
           
                    document.getElementsByClassName("casa")[i].innerHTML = "O";
                    contadorDeJogadas++;
                    vezdojogador=jogador1;
                     
               }
               
                //atualiza o mostrador de quem é a vez
               vezdequem();
               //verifica se alguém ganhou
               ganhou();
               }
                     
          }

     } 

}


// Zera todos as posições e recomeça o jogo
function resetar(){
     //zera o jogo
     var casas = document.getElementsByClassName("casa");
     for( var i=0;i<9;i++){
          casas[i].innerHTML = "";
     }
     //volta as variáveis para os seus valores iniciáis
     contadorDeJogadas = 0;
     vezdojogador = jogador1;
     //apaga o nome do vencedor anterior
     document.getElementById("indicadorVencedor").innerHTML="";
     //ativa o jogo novamente
     fimdojogo = false;
     vezdequem();
  
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
     // pegando os valores de cada botão pela sua id
   var p0 = document.getElementById("p0").innerHTML;  
   var p1 = document.getElementById("p1").innerHTML;  
   var p2 = document.getElementById("p2").innerHTML;  
   var p3 = document.getElementById("p3").innerHTML;  
   var p4 = document.getElementById("p4").innerHTML;  
   var p5 = document.getElementById("p5").innerHTML;  
   var p6 = document.getElementById("p6").innerHTML;  
   var p7 = document.getElementById("p7").innerHTML;  
   var p8 = document.getElementById("p8").innerHTML;  

   var vencedor ="";
// verificando as condições de vitória
   if( (p0 == p3 && p0 == p6 && p0 != "") || (p0 == p1 && p0 == p2 && p0 != "") || (p0 == p4 && p0 == p8 && p0!= "") ) {
        vencedor= p0;
   } else if( (p4 == p3 && p4 == p5 && p4 != "") || (p4 == p1 && p4 == p7 && p4 != "") || (p4 == p2 && p4 == p6 && p4!= "") ) {
        vencedor = p4;
   } else if ( (p8 == p7 && p8 == p6 && p8 != "") || (p8 == p2 && p8 == p5 && p8 !="") ) {
        vencedor = p8;
   }
// encerrando o jogo e indicando o vencedor
   if(vencedor!="") {
        fimdojogo = true;
        document.getElementById("indicadorVencedor").innerHTML="O vencedor é : "+ vencedor;
   }
// caso o numero de jogadas for 9, ou seja todos os botões estão preenchidos, e ainda não tiver vencedor, ele avisa que Deu Velha
   else if ( vencedor =="" && contadorDeJogadas == 9) {
        fimdojogo = true;
     document.getElementById("indicadorVencedor").innerHTML="Deu Velha";
   }
   
  
}